const slash = require(`slash`)
const path = require(`path`)
const { readFileSync, stat } = require(`fs-extra`)
const mime = require(`mime`)
const prettyBytes = require(`pretty-bytes`)
// const { getImageSize } = require(`./../gatsby-plugin-sharp`)
const imageSize = require(`probe-image-size`)

const md5File = require(`bluebird`).promisify(require(`md5-file`))
const crypto = require(`crypto`)

const createId = path => {
  const slashed = slash(path)
  return `${slashed} absPath of file`
}

exports.createId = createId

exports.createFileNode = async ({
  path: pathToFile,
  reporter,
  pluginOptions = {},
}) => {
  const slashed = slash(pathToFile)
  const parsedSlashed = path.parse(slashed)
  const slashedFile = {
    ...parsedSlashed,
    absolutePath: slashed,
    // Useful for limiting graphql query with certain parent directory
    relativeDirectory: path.relative(
      pluginOptions.path || process.cwd(),
      parsedSlashed.dir
    ),
  }

  const stats = await stat(slashedFile.absolutePath)
  let internal
  let imageInfo
  if (stats.isDirectory()) {
    const contentDigest = crypto
      .createHash(`md5`)
      .update(
        JSON.stringify({ stats: stats, absolutePath: slashedFile.absolutePath })
      )
      .digest(`hex`)
    internal = {
      contentDigest,
      type: `Directory`,
      description: `Directory "${path.relative(process.cwd(), slashed)}"`,
    }
  } else {
    const mediaType = mime.lookup(slashedFile.ext)
    if (mediaType.indexOf(`image/`) === 0) {
      const { width, height } = imageSize.sync(
        readFileSync(slashedFile.absolutePath)
      )
      const aspectRatio = `${(width / height).toFixed(2).replace(`.00`, ``)}`
      imageInfo = {
        aspectRatio,
        width,
        height,
      }
    }
    const contentDigest = await md5File(slashedFile.absolutePath)
    internal = {
      contentDigest,
      mediaType,
      type: `File`,
      description: `File "${path.relative(process.cwd(), slashed)}"`,
    }
  }

  // console.log('createFileNode:stat', slashedFile.absolutePath)
  // Stringify date objects.
  return JSON.parse(
    JSON.stringify({
      // Don't actually make the File id the absolute path as otherwise
      // people will use the id for that and ids shouldn't be treated as
      // useful information.
      id: createId(pathToFile),
      children: [],
      parent: `___SOURCE___`,
      internal,
      sourceInstanceName: pluginOptions.name || `__PROGRAMATTIC__`,
      absolutePath: slashedFile.absolutePath,
      relativePath: slash(
        path.relative(
          pluginOptions.path || process.cwd(),
          slashedFile.absolutePath
        )
      ),
      extension: slashedFile.ext.slice(1).toLowerCase(),
      size: stats.size,
      prettySize: prettyBytes(stats.size),
      modifiedTime: stats.mtime,
      accessTime: stats.atime,
      changeTime: stats.ctime,
      birthTime: stats.birthtime,
      ...slashedFile,
      ...stats,
      ...imageInfo,
    })
  )
}
