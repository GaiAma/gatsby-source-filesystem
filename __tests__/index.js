"use strict";

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

var path = require("path");

var _require = require("../"),
    loadNodeContent = _require.loadNodeContent;

describe("gatsby-source-filesystem", function () {
  it("can load the content of a file",
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var content;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return loadNodeContent({
              absolutePath: path.join(__dirname, "../index.js")
            });

          case 2:
            content = _context.sent;
            expect(content.length).toBeGreaterThan(0);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  })));
  it("rejects if file not found",
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return loadNodeContent({
              absolutePath: path.join(__dirname, "haha-not-a-real-file.js")
            }).catch(function (err) {
              expect(err).toBeDefined();
            });

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  })));
});