"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

var fs = require("fs-extra");

var got = require("got");

var crypto = require("crypto");

var path = require("path");

var _require = require("valid-url"),
    isWebUri = _require.isWebUri;

var Queue = require("better-queue");

var _require2 = require("./create-file-node"),
    createFileNode = _require2.createFileNode;

var cacheId = function cacheId(url) {
  return "create-remote-file-node-".concat(url);
};
/********************
 * Type Definitions *
 ********************/

/**
 * @typedef {Redux}
 * @see [Redux Docs]{@link https://redux.js.org/api-reference}
 */

/**
 * @typedef {GatsbyCache}
 * @see gatsby/packages/gatsby/utils/cache.js
 */

/**
 * @typedef {Auth}
 * @type {Object}
 * @property {String} htaccess_pass
 * @property {String} htaccess_user
 */

/**
 * @typedef {CreateRemoteFileNodePayload}
 * @typedef {Object}
 * @description Create Remote File Node Payload
 *
 * @param  {String} options.url
 * @param  {Redux} options.store
 * @param  {GatsbyCache} options.cache
 * @param  {Function} options.createNode
 * @param  {Auth} [options.auth]
 */

/*********
 * utils *
 *********/

/**
 * createHash
 * --
 *
 * Create an md5 hash of the given str
 * @param  {Stringq} str
 * @return {String}
 */


var createHash = function createHash(str) {
  return crypto.createHash("md5").update(str).digest("hex");
};

var CACHE_DIR = ".cache";
var FS_PLUGIN_DIR = "gatsby-source-filesystem";
/**
 * createFilePath
 * --
 *
 * @param  {String} directory
 * @param  {String} filename
 * @param  {String} url
 * @return {String}
 */

var createFilePath = function createFilePath(directory, filename, ext) {
  return path.join(directory, CACHE_DIR, FS_PLUGIN_DIR, "".concat(filename).concat(ext));
};
/********************
 * Queue Management *
 ********************/

/**
 * Queue
 * Use the task's url as the id
 * When pushing a task with a similar id, prefer the original task
 * as it's already in the processing cache
 */


var queue = new Queue(pushToQueue, {
  id: "url",
  merge: function merge(old, _, cb) {
    return cb(old);
  },
  concurrent: 200
});
/**
 * @callback {Queue~queueCallback}
 * @param {*} error
 * @param {*} result
 */

/**
 * pushToQueue
 * --
 * Handle tasks that are pushed in to the Queue
 *
 *
 * @param  {CreateRemoteFileNodePayload}          task
 * @param  {Queue~queueCallback}  cb
 * @return {Promise<null>}
 */

function pushToQueue(_x, _x2) {
  return _pushToQueue.apply(this, arguments);
}
/******************
 * Core Functions *
 ******************/

/**
 * requestRemoteNode
 * --
 * Download the requested file
 *
 * @param  {String}   url
 * @param  {Headers}  headers
 * @param  {String}   tmpFilename
 * @param  {String}   filename
 * @return {Promise<Object>}  Resolves with the [http Result Object]{@link https://nodejs.org/api/http.html#http_class_http_serverresponse}
 */


function _pushToQueue() {
  _pushToQueue = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(task, cb) {
    var node;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return processRemoteNode(task);

          case 3:
            node = _context.sent;
            return _context.abrupt("return", cb(null, node));

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            return _context.abrupt("return", cb(null, _context.t0));

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 7]]);
  }));
  return _pushToQueue.apply(this, arguments);
}

var requestRemoteNode = function requestRemoteNode(url, headers, tmpFilename, filename) {
  return new Promise(function (resolve, reject) {
    var responseStream = got.stream(url, _objectSpread({}, headers, {
      timeout: 30000,
      retries: 5
    }));
    var fsWriteStream = fs.createWriteStream(tmpFilename);
    responseStream.pipe(fsWriteStream);
    responseStream.on("downloadProgress", function (pro) {
      return console.log(pro);
    }); // If there's a 400/500 response or other error.

    responseStream.on("error", function (error, body, response) {
      fs.removeSync(tmpFilename);
      reject(error);
    });
    fsWriteStream.on("error", function (error) {
      reject(error);
    });
    responseStream.on("response", function (response) {
      fsWriteStream.on("finish", function () {
        resolve(response);
      });
    });
  });
};
/**
 * processRemoteNode
 * --
 * Request the remote file and return the fileNode
 *
 * @param {CreateRemoteFileNodePayload} options
 * @return {Promise<Object>} Resolves with the fileNode
 */


function processRemoteNode(_x3) {
  return _processRemoteNode.apply(this, arguments);
}
/**
 * Index of promises resolving to File node from remote url
 */


function _processRemoteNode() {
  _processRemoteNode = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(_ref) {
    var url, store, cache, createNode, _ref$auth, auth, programDir, cachedHeaders, headers, digest, ext, tmpFilename, filename, response, fileNode;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            url = _ref.url, store = _ref.store, cache = _ref.cache, createNode = _ref.createNode, _ref$auth = _ref.auth, auth = _ref$auth === void 0 ? {} : _ref$auth;
            // Ensure our cache directory exists.
            programDir = store.getState().program.directory;
            _context2.next = 4;
            return fs.ensureDir(path.join(programDir, CACHE_DIR, FS_PLUGIN_DIR));

          case 4:
            _context2.next = 6;
            return cache.get(cacheId(url));

          case 6:
            cachedHeaders = _context2.sent;
            headers = {}; // Add htaccess authentication if passed in. This isn't particularly
            // extensible. We should define a proper API that we validate.

            if (auth && auth.htaccess_pass && auth.htaccess_user) {
              headers.auth = "".concat(auth.htaccess_user, ":").concat(auth.htaccess_pass);
            }

            if (cachedHeaders && cachedHeaders.etag) {
              headers["If-None-Match"] = cachedHeaders.etag;
            } // Create the temp and permanent file names for the url.


            digest = createHash(url);
            ext = path.parse(url).ext;
            tmpFilename = createFilePath(programDir, "tmp-".concat(digest), ext);
            filename = createFilePath(programDir, digest, ext); // Fetch the file.

            _context2.prev = 14;
            _context2.next = 17;
            return requestRemoteNode(url, headers, tmpFilename, filename);

          case 17:
            response = _context2.sent;
            // Save the response headers for future requests.
            cache.set(cacheId(url), response.headers); // If the status code is 200, move the piped temp file to the real name.

            if (!(response.statusCode === 200)) {
              _context2.next = 24;
              break;
            }

            _context2.next = 22;
            return fs.move(tmpFilename, filename, {
              overwrite: true
            });

          case 22:
            _context2.next = 26;
            break;

          case 24:
            _context2.next = 26;
            return fs.remove(tmpFilename);

          case 26:
            _context2.next = 28;
            return createFileNode(filename, {});

          case 28:
            fileNode = _context2.sent;
            fileNode.internal.description = "File \"".concat(url, "\""); // Override the default plugin as gatsby-source-filesystem needs to
            // be the owner of File nodes or there'll be conflicts if any other
            // File nodes are created through normal usages of
            // gatsby-source-filesystem.

            createNode(fileNode, {
              name: "gatsby-source-filesystem"
            });
            return _context2.abrupt("return", fileNode);

          case 34:
            _context2.prev = 34;
            _context2.t0 = _context2["catch"](14);

          case 36:
            return _context2.abrupt("return", null);

          case 37:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this, [[14, 34]]);
  }));
  return _processRemoteNode.apply(this, arguments);
}

var processingCache = {};
/**
 * pushTask
 * --
 * pushes a task in to the Queue and the processing cache
 *
 * Promisfy a task in queue
 * @param {CreateRemoteFileNodePayload} task
 * @return {Promise<Object>}
 */

var pushTask = function pushTask(task) {
  return new Promise(function (resolve, reject) {
    queue.push(task).on("finish", function (task) {
      resolve(task);
    }).on("failed", function () {
      resolve();
    });
  });
};
/***************
 * Entry Point *
 ***************/

/**
 * createRemoteFileNode
 * --
 *
 * Download a remote file
 * First checks cache to ensure duplicate requests aren't processed
 * Then pushes to a queue
 *
 * @param {CreateRemoteFileNodePayload} options
 * @return {Promise<Object>}                  Returns the created node
 */


module.exports = function (_ref2) {
  var url = _ref2.url,
      store = _ref2.store,
      cache = _ref2.cache,
      createNode = _ref2.createNode,
      _ref2$auth = _ref2.auth,
      auth = _ref2$auth === void 0 ? {} : _ref2$auth;

  // Check if we already requested node for this remote file
  // and return stored promise if we did.
  if (processingCache[url]) {
    return processingCache[url];
  }

  if (!url || isWebUri(url) === undefined) {
    // should we resolve here, or reject?
    // Technically, it's invalid input
    return Promise.resolve();
  }

  return processingCache[url] = pushTask({
    url: url,
    store: store,
    cache: cache,
    createNode: createNode,
    auth: auth
  });
};