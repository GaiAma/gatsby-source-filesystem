"use strict";

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

var slash = require("slash");

var path = require("path");

var _require = require("fs-extra"),
    readFileSync = _require.readFileSync,
    stat = _require.stat;

var mime = require("mime");

var prettyBytes = require("pretty-bytes"); // const { getImageSize } = require(`./../gatsby-plugin-sharp`)


var imageSize = require("probe-image-size");

var md5File = require("bluebird").promisify(require("md5-file"));

var crypto = require("crypto");

var createId = function createId(path) {
  var slashed = slash(path);
  return "".concat(slashed, " absPath of file");
};

exports.createId = createId;

exports.createFileNode =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(_ref) {
    var pathToFile, reporter, _ref$pluginOptions, pluginOptions, slashed, parsedSlashed, slashedFile, stats, internal, imageInfo, contentDigest, mediaType, _imageSize$sync, width, height, aspectRatio, _contentDigest;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            pathToFile = _ref.path, reporter = _ref.reporter, _ref$pluginOptions = _ref.pluginOptions, pluginOptions = _ref$pluginOptions === void 0 ? {} : _ref$pluginOptions;
            slashed = slash(pathToFile);
            parsedSlashed = path.parse(slashed);
            slashedFile = _objectSpread({}, parsedSlashed, {
              absolutePath: slashed,
              // Useful for limiting graphql query with certain parent directory
              relativeDirectory: path.relative(pluginOptions.path || process.cwd(), parsedSlashed.dir)
            });
            _context.next = 6;
            return stat(slashedFile.absolutePath);

          case 6:
            stats = _context.sent;

            if (!stats.isDirectory()) {
              _context.next = 12;
              break;
            }

            contentDigest = crypto.createHash("md5").update(JSON.stringify({
              stats: stats,
              absolutePath: slashedFile.absolutePath
            })).digest("hex");
            internal = {
              contentDigest: contentDigest,
              type: "Directory",
              description: "Directory \"".concat(path.relative(process.cwd(), slashed), "\"")
            };
            _context.next = 18;
            break;

          case 12:
            mediaType = mime.lookup(slashedFile.ext);

            if (mediaType.indexOf("image/") === 0) {
              _imageSize$sync = imageSize.sync(readFileSync(slashedFile.absolutePath)), width = _imageSize$sync.width, height = _imageSize$sync.height;
              aspectRatio = "".concat((width / height).toFixed(2).replace(".00", ""));
              imageInfo = {
                aspectRatio: aspectRatio,
                width: width,
                height: height
              };
            }

            _context.next = 16;
            return md5File(slashedFile.absolutePath);

          case 16:
            _contentDigest = _context.sent;
            internal = {
              contentDigest: _contentDigest,
              mediaType: mediaType,
              type: "File",
              description: "File \"".concat(path.relative(process.cwd(), slashed), "\"")
            };

          case 18:
            return _context.abrupt("return", JSON.parse(JSON.stringify(_objectSpread({
              // Don't actually make the File id the absolute path as otherwise
              // people will use the id for that and ids shouldn't be treated as
              // useful information.
              id: createId(pathToFile),
              children: [],
              parent: "___SOURCE___",
              internal: internal,
              sourceInstanceName: pluginOptions.name || "__PROGRAMATTIC__",
              absolutePath: slashedFile.absolutePath,
              relativePath: slash(path.relative(pluginOptions.path || process.cwd(), slashedFile.absolutePath)),
              extension: slashedFile.ext.slice(1).toLowerCase(),
              size: stats.size,
              prettySize: prettyBytes(stats.size),
              modifiedTime: stats.mtime,
              accessTime: stats.atime,
              changeTime: stats.ctime,
              birthTime: stats.birthtime
            }, slashedFile, stats, imageInfo))));

          case 19:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref2.apply(this, arguments);
  };
}();